<?php
  class Menus extends CI_Controller
  {
      //constructor
      function __construct()
      {
      parent::__construct();
      }
      //renderizacion o representacion grafica de la vista que muestra los desayunos
      public function desayunos()
          {
          $this->load->view('header');
          $this->load->view('menus/desayunos');
          $this->load->view('footer');
          }

        //renderizacion de la vista que muestra los desayunos
          public function almuerzos()
          {
           $this->load->view('header');
           $this->load->view('menus/almuerzos');
           $this->load->view('footer');
          }
          public function meriendas()
          {
           $this->load->view('header');
           $this->load->view('menus/meriendas');
           $this->load->view('footer');
          }
  } // no borrar llave de cierre la clase
?>
